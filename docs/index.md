# Example Repo

Example Repo used as a Tempalte for Uni Writeups.

## Supports Draw.io Diagrams

![](./graphs/example.drawio)

## Supports Mermaid

```mermaid
sequenceDiagram
    participant A as Client
    participant B as Server

    A->>B: SYN
    B-->>A: SYN+ACK
    A->>B: ACK
```

## Supports MkDocs Material

!!! info ""
    Admonitions

!!! warn "Admonitions with Names"
    ??? success "Nested Admonitions"
        :)

=== "Tab A"
    Content of Tab A
=== "Tab B"
    Content of Tab B

## Icons and Emojis

:smile: - Emojis

:octicons-alert-16: - Octicons

and some more, see [Documentation :octicons-link-external-16:](https://squidfunk.github.io/mkdocs-material/reference/icons-emojis/){target=_blank}

## MathJax

See [Documentation :octicons-link-external-16:](https://squidfunk.github.io/mkdocs-material/reference/mathjax/#usage){target=_blank}
### Inline
Text bla bla bla $f(x)=x^2$ bla bla.

### Multiline
$$
x = {-b \pm \sqrt{b^2-4ac} \over 2a}
$$


